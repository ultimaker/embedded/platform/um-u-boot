# Introduction
This repository contains the Ultimaker specific um-u-boot package source code. There are separate branches for differing SOM hardware available on Ultimaker Printers.
This branch is specifically for u-boot code that is compatible with the MSC imx6 chipset.
As of Aug 2022, this branch can support MSC imx6 revisions 03A and 03E. The source is the original (private) MSC customized U-Boot version. It is cloned in our Ultimaker repo: git@github.com:Ultimaker/u-boot-denx-msc.git (it is a submodule of the um-u-boot repo).

#### Scripts directory:
Contains pre- and post-package install scripts. These are mainly concerned with reading the correct article number and selecting the correct image to be displayed on the splash screen.


## How to build
This repository is setup to use an existing *Docker* image from this repositories Docker registry.
The image contains all the tools to be able to build the software. When Docker is not installed
the build script will try to build on the host machine and is likely to fail because the required
tools are not installed. When it is necessary to run the build without Docker, execute the
'buildenv_check.sh' script and see if the environment is missing requirements.

By default the build script runs an environment check, builds the image and then validates
it by running tests. The first and the latter can be disabled. Discover the usage of the
'build_for_ultimaker.sh' script by using the `-h` option.
Example build command and output of `-h` shown below
```sh
> RELEASE_VERSION=<major.minor.patch-[dev]> ./build_for_ultimaker.sh
    Usage: ./build_for_ultimaker.sh [OPTIONS]
        -c   Skip run of build environment checks
        -h   Print usage
        -l   skip code linting
        -t   Skip run of rootfs tests
```
